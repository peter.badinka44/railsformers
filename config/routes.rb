Rails.application.routes.draw do
  devise_for :users
  root "search#index"

  resources :articles do
    resources :comments
  end
end
