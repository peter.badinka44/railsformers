class SearchController < ApplicationController
	def index
		@q = Article.ransack(params[:q])
		@articles = Kaminari.paginate_array(@q.result).page(params[:page]).per(3)
	end
end
